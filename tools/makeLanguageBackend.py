#!/usr/bin/env python3

import configparser
import sys
import os.path
import time
from subprocess import call

class LanguageBackend:
    def __init__(self, directory, language):
        self.directory = directory
        self.language = language
        self.lexicas = []
        self.pattern = None

    def append(self, file):
        path, ext = os.path.splitext(file)
        name = os.path.basename(path)
        outfile = self._getFile(name, ext)

        if ext == ".fbdic":
            self.lexicas.append((file, outfile, name))
        elif ext == ".pat":
            if self.pattern:
                raise "Multiple pattern files"
            self.patterns = (file, outfile)

    def create(self):
        if not self.patterns:
            raise "Missing pattern file"
        if len(self.lexicas) == 0:
            raise "Missing lexicon file"

        self._mkdir()
        self._copy(self.patterns)
        for lex in self.lexicas:
            self._copy(lex)
        self._createIniFile()

    def _createIniFile(self):
        config = configparser.RawConfigParser()
        config.optionxform = str
        config.add_section("global")
        config.set("global", "creationTime", time.strftime("%H:%M:%S %d.%m.%Y"))
        config.set("global", "numberOfIterations", 10)
        config.set("global", "patternCutoff_hist", 0.4)
        config.set("global", "patternCutoff_ocr", 0.05)
        config.set("global", "histPatternSmoothingProb", 1e-5)
        config.set("global", "ocrPatternSmoothingProb", 1e-5)
        config.set("global", "resetHistPatternProbabilities", 0)
        config.set("global", "resetOCRPatternProbabilities", 0)
        config.set("global", "ocrPatternStartProb", 1e-5)
        config.set("global", "donttouch_hyphenation", 1)
        config.set("global", "donttouch_lineborders", 0)

        config.set("global", "resetOCRPatternProbabilities", 0)

        config.add_section("language_model")
        config.set("language_model", "patternFile", self._getPatternsFile())
        config.set("language_model", "patternWeightsFile", self._getWeightsFile())
        config.set("language_model", "freqListFile", self._getFreqlistFile())
        config.set("language_model", "corpusLexicon", self._getCorpusLexiconFile())

        config.add_section("dictionaries")
        tmp = ""
        for lex in self.lexicas:
            tmp += lex[2] + " "
        config.set("dictionaries", "activeDictionaries", tmp.strip())

        priority = 100
        for lex in self.lexicas:
            name = lex[2]
            config.add_section(name)
            config.set(name, "path", self._getFile(name, ".fbdic"))
            config.set(name, "histPatterns", 2)
            config.set(name, "ocrErrors", 2)
            config.set(name, "ocrErrorsOnHypothetic", 1)
            config.set(name, "priority", priority)
            config.set(name, "cascadeRank", 0)
            priority -= 10

        with open(self._getIniFile(), "w") as out:
            config.write(out)

    def _getFile(self, name, ext):
        return os.path.join(self._getLanguageDir(), name + ext)
    def _getIniFile(self):
        return os.path.join(self._getBackendDir(), self.language + ".ini")
    def _getPatternsFile(self):
        return os.path.join(self._getLanguageDir(), os.path.basename(self.patterns[1]))
    def _getWeightsFile(self):
        return self._getFile("patternWeights", ".txt")
    def _getCorpusLexiconFile(self):
        return self._getFile("corpusLexicon", ".txt")
    def _getFreqlistFile(self):
        return self._getFile(self.language, ".binfreq")
    def _getBackendDir(self):
        return os.path.abspath(self.directory)
    def _getLanguageDir(self):
        return os.path.join(self._getBackendDir(), self.language)
    def _copy(self, sourceDest):
        args = ["cp", sourceDest[0], sourceDest[1]];
        call(args)
    def _mkdir(self):
        os.makedirs(self._getLanguageDir(), exist_ok=True)


if __name__ == '__main__':
    prog = sys.argv.pop(0)
    if len(sys.argv) < 2:
        print("usage: {} <dir> <language> ...".format(prog))
        sys.exit(1)

    directory = sys.argv.pop(0)
    language = sys.argv.pop(0)
    backend = LanguageBackend(directory, language)
    for file in sys.argv:
        backend.append(file)
    backend.create()
