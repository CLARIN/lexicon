#!/usr/bin/env python3

import configparser
import sys
import os.path
from subprocess import call

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("usage: {} <dir> <language> <file>".format(sys.argv[0]))
        sys.exit(1)
    directory = sys.argv[1]
    language = sys.argv[2]
    infile = sys.argv[3]
    configfile = os.path.join(directory, language + ".ini")
    config = configparser.RawConfigParser()
    config.read(configfile)
    freqs = os.path.join(directory, config.get("language_model", "freqListFile"))
    weights = os.path.join(directory, config.get("language_model", "patternWeightsFile"))
    args = [
        os.path.join(directory, "bin/trainFrequencyList"),
        "--config", configfile,
        "--out_freqlist", freqs,
        "--out_weights", weights,
        "--textFile", infile
    ]
    call(args)
