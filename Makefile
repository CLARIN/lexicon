BACKEND ?= $(PWD)/backend
PROFILER = $(BACKEND)/bin/profiler
COMPILE_FBDIC = $(BACKEND)/bin/compileFBDic
TRAIN_FREQUENCY_LIST = $(BACKEND)/bin/trainFrequencyList

OCRCXX_BASE = gsm/ocrcxx
OCRCXX_MAKEFILE = $(OCRCXX_BASE)/Makefile
OCRCXX_BUILD = $(OCRCXX_BASE)/build
OCRCXX_LIB = $(OCRCXX_BUILD)/lib/libOCRCorrection.so

# PROFILER
CONFIG = tools/config.py
LEX = $(patsubst %.lex,%.fbdic,$(shell find lex -type f -name '*.lex'))
PAT = $(shell find lex -type f -name '*.pat')

default: backend

binaries: $(PROFILER) $(COMPILE_FBDIC) $(TRAIN_FREQUENCY_LIST)

$(OCRCXX_LIB): $(OCRCXX_MAKEFILE)
	$(MAKE) -C $(OCRCXX_BASE)

$(PROFILER) $(COMPILE_FBDIC) $(TRAIN_FREQUENCY_LIST): $(OCRCXX_LIB)
	$(MAKE) -C $(OCRCXX_BASE) PREFIX=/bin DESTDIR=$(BACKEND) install

.PRECIOUS: %.sorted
%.sorted: %.lex
	LC_ALL=C sort $< | uniq > $@

.PRECIOUS: %.fbdic
%.fbdic: %.sorted $(COMPILE_FBDIC)
	$(COMPILE_FBDIC) $< $@

backend: $(addsuffix .ini,$(addprefix $(BACKEND)/,$(notdir $(shell find lex/* -type d))))
	echo $^

.SECONDEXPANSION:
%.ini: LANGUAGE = $(patsubst $(BACKEND)/%.ini,%,$@)
%.ini: DEPS = $(filter lex/$(LANGUAGE)/%,$(LEX) $(PAT))
%.ini: $$(DEPS) $(TRAIN_FREQUENCY_LIST)
	tools/makeLanguageBackend.py $(BACKEND) $(LANGUAGE) $(DEPS)
	$(TRAIN_FREQUENCY_LIST) --config $@ --textFile lorem.txt

.PHONY: clean
clean:
	find lex -type f -name '*.sorted' -exec rm {} \; -o -name '*.fbdic' -exec rm {} \;
