# CLARIN/lexicon
This repository stores lexical resources for various languages.
You can use these resources to build a language backend for
[ProfilerWebService](https://gitlab.cis.uni-muenchen.de/CLARIN/pws).

Dependencies:
- [git](http://www.git-scm.com/)
- [GNU make](https://www.gnu.org/software/make/)
- [gcc](https://gcc.gnu.org/)
- [cppunit](http://freedesktop.org/wiki/Software/cppunit/)
- [xerces-c](https://xerces.apache.org/xerces-c/)

# Basic directory structure

- README.md: this file
- tools: contains tools to build the language backend
- Makefile: controls the building of the language backend
- lex: contains various lexical resources

# Building the language backend
Before  you can build the language backend you have to initialize the git submodules
using `git submodule update --init --recursive`.
In order to build the language backend,
make shure that you have installed all additional dependencies.
Then just type `make BACKEND=/path/to/backend/dir backend`

The above command first compiles all the needed tools to build the language backend
and afterwards compiles all lexical ressources needed by the language profiler.
All ressources are stored under the given backend directory in the make command.

*Note: you should not copy the resulting directory
since all files are dependent on the full path of the backend*

## Structure of the lex directory
The lex directory contains dictionaries, pattern files
and (historical) ground truth training copora.
Ressources for each language *must* reside in their own subdirectory
and are identified by the name of the subdirectory.

Dictionaries contain the full forms of the language.
The files must be utf-8 encoded and have the extension `.lex`.
Each language subdirectory can contain multiple dictionary files.

The pattern file contains rules of orthographical variation.
It must have the extension `.pat`.
Each rule in the file needs to be on its own line.
Each language subdirectory can only contain *one* such file.

*missing description of training files*
